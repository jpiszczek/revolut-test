val scalaOpts = Seq("-unchecked", "-feature", "-deprecation", "-Ywarn-unused", "-Xfatal-warnings",
  "-Ywarn-inaccessible", "-Ywarn-infer-any", "-Ywarn-nullary-override", "-Ywarn-nullary-unit",
  "-Ywarn-dead-code")

val root = project.in(file(".")).settings(
  name := "transfer-service",
  scalaVersion := "2.12.7",
  organization := "com.kubapiszczek"
).aggregate(model, routes, api, server)

lazy val model = project
  .settings(
    libraryDependencies ++= Seq(
      "joda-time" % "joda-time" % "2.10.1",
      "io.circe" %% "circe-core" % "0.10.1",
      "io.circe" %% "circe-parser" % "0.10.1",
      "io.circe" %% "circe-generic" % "0.10.1",
      "io.circe" %% "circe-generic-extras" % "0.10.1"
    )
  )

lazy val routes = project
  .settings(
    scalaVersion := "2.12.7",
    name := "routes",
    scalacOptions ++= scalaOpts,
    libraryDependencies ++= Seq(
      "com.twitter" %% "finagle-core" % "18.10.0",
      "com.twitter" %% "finagle-http" % "18.10.0",
      ("io.fintrospect" %% "fintrospect-core" % "14.22.0")
        .exclude("com.twitter", "finagle-core")
        .exclude("com.twitter", "finagle-http"),
      "io.fintrospect" %% "fintrospect-circe" % "14.22.0"
    ),
    assemblyMergeStrategy in assembly := {
      case PathList("META-INF", xs @ _*) => MergeStrategy.discard
      case x => MergeStrategy.first
    }
  ).dependsOn(model)

lazy val storage = project
  .settings(
    scalacOptions ++= scalaOpts,
    libraryDependencies ++= Seq(
      "org.scalikejdbc" %% "scalikejdbc" % "2.5.2",
      "com.h2database" % "h2" % "1.4.197",
      "com.twitter" %% "util-core" % "18.10.0",
      "org.scalatest" %% "scalatest" % "3.0.5" % Test
    ),
  ).dependsOn(model)

lazy val api = project
  .settings(
    scalacOptions ++= scalaOpts,
    libraryDependencies ++= Seq(
      "org.scalatest" %% "scalatest" % "3.0.5" % Test
    ),
    assemblyMergeStrategy in assembly := {
      case PathList("META-INF", xs @ _*) => MergeStrategy.discard
      case x => MergeStrategy.first
    }
  ).dependsOn(routes, storage)

lazy val server = project
  .settings(
    scalacOptions ++= scalaOpts,
    mainClass := Option("com.kubapiszczek.transfer.server"),
    libraryDependencies ++= Seq(
      "com.twitter" %% "twitter-server" % "18.10.0"
    ),
    assemblyMergeStrategy in assembly := {
      case PathList("META-INF", xs @ _*) => MergeStrategy.discard
      case x => MergeStrategy.first
    }
  ).dependsOn(api)