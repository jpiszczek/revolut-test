package com.kubapiszczek.transfer.api

import java.util.UUID

import com.kubapiszczek.transfer.model._
import com.kubapiszczek.transfer.model.requests.{AccountRequest, TransactionRequest}
import com.kubapiszczek.transfer.routes
import com.kubapiszczek.transfer.storage.Storage
import com.twitter.finagle.Service
import com.twitter.finagle.http.path.Root
import com.twitter.finagle.http.{Request, Response}
import com.twitter.util.Future
import io.fintrospect.{RouteModule, ServerRoute}
import io.fintrospect.formats.Circe.Auto._

class RoutesImpl(storage: Storage) {
  private def getAccountImpl(accountID: UUID): Service[Request, Response] = {
    val svc: Service[Request, Account] = Service mk { _ =>
      val result = storage getAccount accountID
      Future const result
    }
    Out(svc)
  }

  private val getAccount: ServerRoute[Request, Response] =
    routes.getAccount bindTo (id => filters.errorToResponse andThen getAccountImpl(id))

  private def createAccountImpl(id: UUID): Service[Request, Response] = Service mk { req => {
    val svc = InOut(Service mk { accountReq: AccountRequest =>
      val account = Account(id, accountReq.balance, accountReq.currency)
      val result = storage createAccount account
      Future const result map (_ => account)
    })
    svc(req)
  }}

  private val createAccount: ServerRoute[Request, Response] =
    routes.createAccount bindTo (id => filters.errorToResponse andThen createAccountImpl(id))

  private def createTransactionImpl(accountID: UUID, transactionID: UUID):  Service[Request, Response] = Service mk { req => {
    val svc = InOut(Service mk { transactionReq: TransactionRequest =>
      val transaction = Transaction(fromAccount = accountID, toAccount = transactionReq.toAccount, id = transactionID, delta = transactionReq.delta)
      val result = storage transferAmount transaction
      Future const result map (_ => transaction)
    })
    svc(req)
  }}

  private val createTransaction: ServerRoute[Request, Response] =
    routes.transferFunds bindTo ((accountID, _, transactionID) => filters.errorToResponse andThen createTransactionImpl(accountID, transactionID))

  private def getTransactionsImpl(accountID: UUID): Service[Request, Response] = {
    val svc: Service[Request, List[Transaction]] = Service mk { _ =>
      val result = storage getAccountsTransactions accountID
      Future const result
    }
    Out(svc)
  }

  private val getTransactions: ServerRoute[Request, Response] =
    routes.getTransactions bindTo ((accountID, _) => filters.errorToResponse andThen getTransactionsImpl(accountID))

  val service: Service[Request, Response] = RouteModule(Root)
    .withRoute(
      getAccount,
      createAccount,
      createTransaction,
      getTransactions
    ).toService
}
