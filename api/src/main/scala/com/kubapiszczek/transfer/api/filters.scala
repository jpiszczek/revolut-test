package com.kubapiszczek.transfer.api

import com.kubapiszczek.transfer.model.errors._
import com.twitter.finagle.Filter
import com.twitter.finagle.http.{Request, Response, Status}
import com.twitter.io.Buf
import com.kubapiszczek.transfer.model._
import com.twitter.util.Future
import io.circe.{Json, JsonObject}

object filters {
  private def jsonResponse(code: Int, json: Json) = {
    val response = Response(Status fromCode code)
    response.setContentTypeJson()
    response.content(Buf.Utf8(json.noSpaces))
    Future value response
  }

  private def serviceErrorResponse(code: Int, error: ServiceError) =
    jsonResponse(code, encodeServiceError(error))

  val errorToResponse: Filter[Request, Response, Request, Response] = Filter mk { (req, svc) =>
    svc(req) rescue {
      case error: AccountDoesNotExist     => serviceErrorResponse(404, error)
      case error: TransactionAlreadyExist => serviceErrorResponse(409, error)
      case error: InsufficientFunds       => serviceErrorResponse(409, error)
      case error: IncompatibleCurrency    => serviceErrorResponse(400, error)
      case error: AccountAlreadyExist     => serviceErrorResponse(409, error)
      case t: Throwable =>
        val json = Json fromJsonObject (JsonObject singleton ("message", Json fromString s"$t: ${t.getMessage}"))
        jsonResponse(500, json)
    }
  }
}
