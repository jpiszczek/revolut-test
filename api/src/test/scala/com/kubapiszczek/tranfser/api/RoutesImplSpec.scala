package com.kubapiszczek.tranfser.api

import java.util.UUID

import com.kubapiszczek.transfer.api.RoutesImpl
import com.kubapiszczek.transfer.model._
import com.kubapiszczek.transfer.model.errors.InsufficientFunds
import com.kubapiszczek.transfer.storage.Storage
import com.twitter.finagle.Service
import com.twitter.finagle.http.{Request, RequestBuilder, Response}
import com.twitter.io.Buf
import com.twitter.util.{Await, Future}
import org.scalatest.{Matchers, fixture}
import io.circe.parser.parse

class RoutesImplSpec extends fixture.FlatSpec with Matchers {
  behavior of "RoutesImpl service"

  type Svc          = Service[Request, Response]
  type FixtureParam = Svc

  def withFixture(test: OneArgTest) = {
    val storage = Storage()
    test(new RoutesImpl(storage).service)
  }

  private def createAccount(svc: Svc)(account: Account) = {
    val createReq = RequestBuilder().url(s"http://localhost/${account.id}").buildPost(Buf.Utf8(
      s"""{
         |"balance": ${account.balance},
         |"currency": "${account.currency}"
         |}
       """.stripMargin))
    val fCreateResult = svc(createReq)
    Await ready fCreateResult
  }

  private def makeTransfer(svc: Svc)(transaction: Transaction): Future[Response] = {
    val transactionReq = RequestBuilder().url(s"http://localhost/${transaction.fromAccount}/transactions/${transaction.id}").buildPost(Buf.Utf8(
      s"""{
         |"toAccount":"${transaction.toAccount}",
         |"delta": {
         |  "amount": ${transaction.delta.amount},
         |  "currency": "${transaction.delta.currency}"
         |}
         |}""".stripMargin))
    svc(transactionReq)
  }

  it should "return 404 AccountDoesNoExists for not existing account" in { service =>
    val req = RequestBuilder().url(s"http://localhost/${UUID.randomUUID()}").buildGet()

    val fResult = service(req)

    val result = Await result fResult
    result.statusCode shouldBe 404
  }

  it should "create account" in { service =>
    val accountID = UUID.randomUUID()
    val reqBuilder = RequestBuilder().url(s"http://localhost/$accountID")
    val createReq  = reqBuilder.buildPost(Buf.Utf8(
      s"""{
         |"balance":10000,
         |"currency": "EUR"
         |}
       """.stripMargin))

    val fCreateResult = service(createReq)

    val createResult = Await result fCreateResult
    createResult.statusCode shouldBe 200

    val fGetAccount = service(reqBuilder.buildGet())

    val getResult = Await result fGetAccount
    getResult.statusCode shouldBe 200
    parse(getResult.contentString) flatMap (_.as[Account]) should matchPattern {
      case Right(Account(id, balance, currency)) if id == accountID && currency == "EUR" && balance.compare(BigDecimal(10000)) == 0 =>
    }
  }

  it should "create transaction" in { service =>
    val sourceAccountID = UUID.randomUUID()
    val targetAccountID = UUID.randomUUID()
    val transactionID = UUID.randomUUID()
    val baseBalance    = 10000
    val transferAmount = 1000

    val getReq = (id: UUID) => RequestBuilder().url(s"http://localhost/$id").buildGet()

    createAccount(service)(Account(sourceAccountID, baseBalance, "EUR"))
    createAccount(service)(Account(targetAccountID, baseBalance, "EUR"))

    val transaction = Transaction(sourceAccountID, targetAccountID, transactionID, Delta(transferAmount, "EUR"))

    val fTransactionResult = makeTransfer(service)(transaction)

    val transactionResult = Await result fTransactionResult
    transactionResult.statusCode shouldBe 200

    val fSourceAccount = service(getReq(sourceAccountID))
    val fTargetAccount = service(getReq(targetAccountID))

    val sourceAccountResult = Await result fSourceAccount
    val targetAccountResult = Await result fTargetAccount

    parse(sourceAccountResult.contentString) flatMap (_.as[Account]) should matchPattern {
      case Right(Account(id, balance, currency)) if id == sourceAccountID && currency == "EUR" && balance.compare(BigDecimal(baseBalance - transferAmount)) == 0 =>
    }

    parse(targetAccountResult.contentString) flatMap (_.as[Account]) should matchPattern {
      case Right(Account(id, balance, currency)) if id == targetAccountID && currency == "EUR" && balance.compare(BigDecimal(baseBalance + transferAmount)) == 0 =>
    }
  }

  it should "allow only one transaction to take place" in { service =>
    val sourceAccountID = UUID.randomUUID()
    val targetAccountID = UUID.randomUUID()
    val transactionID = UUID.randomUUID()
    val baseBalance    = 10000
    val transferAmount = 1000

    val transaction = Transaction(sourceAccountID, targetAccountID, transactionID, Delta(transferAmount, "EUR"))

    createAccount(service)(Account(sourceAccountID, baseBalance, "EUR"))
    createAccount(service)(Account(targetAccountID, baseBalance, "EUR"))

    val fResult = Future collect Future.parallel(2)(makeTransfer(service)(transaction))

    val result = Await result fResult
    result map (_.statusCode) should contain theSameElementsAs Seq(200, 409)

    val fTransactions = service(RequestBuilder().url(s"http://locahost/$sourceAccountID/transactions").buildGet())

    val transactions = Await result fTransactions

    parse(transactions.contentString) flatMap (_.as[List[Transaction]]) should matchPattern {
      case Right(List(t: Transaction)) if t.id == transactionID && t.delta.amount.compare(BigDecimal(transferAmount)) == 0 =>
    }
  }

  it should "create account once" in { service =>
    val accountID = UUID.randomUUID()
    val account = Account(accountID, BigDecimal(10000), "EUR")

    val fCreateResult = Future collect Future.parallel(2)(createAccount(service)(account))

    val createResult = Await result fCreateResult
    createResult map (_.statusCode) should contain theSameElementsAs Seq(200, 409)
  }

  it should "not transfer amount bigger then account balance" in { service =>
    val sourceAccountID = UUID.randomUUID()
    val targetAccountID = UUID.randomUUID()
    val balance =  BigDecimal(1000)

    val sourceAccount = Account(sourceAccountID, balance, "EUR")
    val targetAccount = Account(targetAccountID, balance, "EUR")
    val transferAmount = balance * 2

    createAccount(service)(sourceAccount)
    createAccount(service)(targetAccount)

    val transaction = Transaction(sourceAccountID, targetAccountID, UUID.randomUUID(), Delta(transferAmount, "EUR"))

    val fResult = makeTransfer(service)(transaction)

    val result = Await result fResult

    result.statusCode shouldBe 409

    parse(result.contentString) flatMap (_.as[InsufficientFunds]) should matchPattern {
      case Right(_: InsufficientFunds) =>
    }
  }
}
