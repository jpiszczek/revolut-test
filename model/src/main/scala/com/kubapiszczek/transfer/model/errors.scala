package com.kubapiszczek.transfer.model

import java.util.UUID

object errors {
  sealed trait ServiceError extends Exception
  case class IncompatibleCurrency(transferCurrency: String, fromCurrency: String, toCurrency: String) extends ServiceError
  case class InsufficientFunds(transferAmount: Delta, availableFunds: Delta)                          extends ServiceError
  case class AccountDoesNotExist(accountID: UUID)                                                     extends ServiceError
  case class TransactionAlreadyExist(transactionID: UUID)                                             extends ServiceError
  case class AccountAlreadyExist(accountID: UUID)                                                     extends ServiceError
}