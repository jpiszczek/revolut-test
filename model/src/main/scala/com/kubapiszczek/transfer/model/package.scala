package com.kubapiszczek.transfer

import java.util.UUID

import com.kubapiszczek.transfer.model.errors._
import com.kubapiszczek.transfer.model.requests.{AccountRequest, TransactionRequest}
import io.circe.{Decoder, Encoder}
import io.circe.generic.extras.Configuration
import io.circe.generic.extras.semiauto._

package object model {
  implicit val customConfig: Configuration = Configuration.default.withDefaults.withDiscriminator("type")

  implicit val decodeUUID: Decoder[UUID] = Decoder.decodeString map UUID.fromString
  implicit val encodeUUID: Encoder[UUID] = Encoder.encodeString contramap (_.toString)

  implicit val decodeDelta: Decoder[Delta] = deriveDecoder
  implicit val encodeDelta: Encoder[Delta] = deriveEncoder

  // responses
  implicit val encodeAccount:     Encoder[Account]     = deriveEncoder
  implicit val decodeAccount:     Decoder[Account]     = deriveDecoder
  implicit val encodeTransaction: Encoder[Transaction] = deriveEncoder
  implicit val decodeTransaction: Decoder[Transaction] = deriveDecoder

  // requests
  implicit val decodeAccountRequest:     Decoder[AccountRequest]     = deriveDecoder
  implicit val encodeAccountRequest:     Encoder[AccountRequest]     = deriveEncoder
  implicit val decodeTransactionRequest: Decoder[TransactionRequest] = deriveDecoder
  implicit val encodeTransactionRequest: Encoder[TransactionRequest] = deriveEncoder

  // domain error responses
  implicit val encodeAccountDoesNotExist:     Encoder[AccountDoesNotExist]     = deriveEncoder
  implicit val encodeInsufficientFunds:       Encoder[InsufficientFunds]       = deriveEncoder
  implicit val encodeTransactionAlreadyExist: Encoder[TransactionAlreadyExist] = deriveEncoder
  implicit val encodeIncompatibleCurrency:    Encoder[IncompatibleCurrency]    = deriveEncoder

  implicit val decodeAccountDoesNotExist:     Decoder[AccountDoesNotExist]     = deriveDecoder
  implicit val decodeInsufficientFunds:       Decoder[InsufficientFunds]       = deriveDecoder
  implicit val decodeTransactionAlreadyExist: Decoder[TransactionAlreadyExist] = deriveDecoder
  implicit val decodeIncompatibleCurrency:    Decoder[IncompatibleCurrency]    = deriveDecoder

  implicit val encodeServiceError: Encoder[ServiceError] = deriveEncoder
}
