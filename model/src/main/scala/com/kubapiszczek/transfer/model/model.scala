package com.kubapiszczek.transfer.model

import java.util.UUID

case class Account(id: UUID, balance: BigDecimal, currency: String)
case class Delta(amount: BigDecimal, currency: String)
case class Transaction(fromAccount: UUID, toAccount: UUID, id: UUID, delta: Delta)