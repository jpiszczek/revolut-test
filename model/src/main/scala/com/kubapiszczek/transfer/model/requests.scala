package com.kubapiszczek.transfer.model

import java.util.UUID

object requests {
  case class AccountRequest(balance: BigDecimal, currency: String)
  case class TransactionRequest(toAccount: UUID, delta: Delta)
}
