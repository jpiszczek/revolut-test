# Money transfer service

# HTTP API
Service requires to usage of UUIDs as identifiers for both accounts and transactions

| Operation | http operation | example request content | example response content |
|--------|--------|------|-------|
| create account | POST: /$accountID | { "balance": 1000, "currency": "EUR" } | |
| get account | GET: /$accountID | | { "balance": 1000.0000, "currency": "EUR", "accountID": "405fc115-9553-4a68-942c-e5e542574248" } |
| transfer money from account | POST: /$accountID/transactions/$transactionID | { "toAccount": "be7024d6-5593-4dc0-b873-289d2e9b7ea2", "delta": { "amount": 1000, "currency": "EUR" } | |
| get transactions for account | GET: /$accountID/transactions | | [{"fromAccount":"2a2aa5ff-9105-47fd-b59f-82db3c9bc00e","toAccount":"1c6654a7-ba61-43ce-939a-e0fcec9a202f","id":"8d854ee1-2f16-489d-87f7-5070d804ea10","delta":{"amount":1000.0000,"currency":"EUR"}}] |

I'm using POST method for creation of both transactions and accounts instead of PUT method since they are immutable - once created they cannot be changed. Consecutive attempts to create a resource will fail with user error and PUT would suggest otherwise. As ids are provided by the external service they are contained in the url instead of request body as it simplifies operation - resource is available exactly where it was created.
In 'real world' I would rather struggle to make every operation possible compatible with PUT semantics as it would allow service mesh to do retries safely, but for sake of simplicity of implementation I'm leaving it here as is ;)

# technologies used
- Finagle
- Fintrospect - for http routing on top finagle
- H2 database in memory storage - as I didn't want to even think about implementing two-phase commit on my own ;)
- TwitterServer
- Circe - json encoding/decoding
- Scalatest

# usage
```sbt "project server" run```
will bind to port 8080
