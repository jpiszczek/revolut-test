package com.kubapiszczek.transfer

import java.util.UUID

import com.kubapiszczek.transfer.model.requests.{AccountRequest, TransactionRequest}
import com.twitter.finagle.http.{Method, Status}
import io.fintrospect.formats.Circe
import io.fintrospect._
import io.fintrospect.parameters.{Body, Path}

object routes {
  val getAccount: UnboundRoute1[UUID] = RouteSpec("retrieve account information")
    .producing(ContentTypes.APPLICATION_JSON)
    .returning(Status.Ok -> "", Status.NotFound -> "")
    .at(Method.Get) / Path.uuid("accountID")

  val createAccount: UnboundRoute1[UUID] = RouteSpec("create account")
    .producing(ContentTypes.APPLICATION_JSON)
    .body(Body.of(Circe.bodySpec[AccountRequest]))
    .returning(Status.Ok -> "", Status.NotFound -> "")
    .at(Method.Post) / Path.uuid("accountID")

  val transferFunds: UnboundRoute3[UUID, String, UUID] = RouteSpec("transfer currency between accounts")
    .producing(ContentTypes.APPLICATION_JSON)
    .body(Body.of(Circe.bodySpec[TransactionRequest]))
    .returning(Status.Ok -> "", Status.NotFound -> "", Status.Conflict -> "")
    .at(Method.Post) / Path.uuid("accountID") / Path.fixed("transactions") / Path.uuid("transactionID")

  val getTransactions: UnboundRoute2[UUID, String] = RouteSpec("get transactions originated from account")
    .producing(ContentTypes.APPLICATION_JSON)
    .returning(Status.Ok -> "")
    .at(Method.Get) / Path.uuid("accountID") / Path.fixed("transactions")
}
