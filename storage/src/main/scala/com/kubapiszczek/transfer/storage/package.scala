package com.kubapiszczek.transfer

import com.kubapiszczek.transfer.model.errors.ServiceError
import com.twitter.util.{Return, Throw, Try}

package object storage {
  implicit class TryFromOption[A](opt: Option[A]) {
    def toTry(ifEmpty: ServiceError): Try[A] = opt.fold[Try[A]](Throw(ifEmpty))(Return.apply)
  }
}
