package com.kubapiszczek.transfer.storage

import java.util.UUID

import com.kubapiszczek.transfer.model._
import com.kubapiszczek.transfer.model.errors._
import com.twitter.util.{Return, Throw, Try}
import org.h2.api.ErrorCode
import org.h2.jdbc.JdbcSQLException
import scalikejdbc._

import scala.language.postfixOps

class Storage private (implicit session: DBSession) {

  private def parseAccount(r: WrappedResultSet) = for {
    id       <- r stringOpt "account_id" map UUID.fromString
    balance  <- r bigDecimalOpt "balance" map (v => BigDecimal(v))
    currency <- r stringOpt "currency"
  } yield Account(id, balance, currency)

  private def parseTransaction(r: WrappedResultSet) = for {
    id          <- r stringOpt "transaction_id" map UUID.fromString
    fromAccount <- r stringOpt "from_account" map UUID.fromString
    toAccount   <- r stringOpt "to_account" map UUID.fromString
    amount      <- r bigDecimalOpt "amount" map (v => BigDecimal(v))
    currency    <- r stringOpt "currency"
  } yield Transaction(fromAccount, toAccount, id, Delta(amount, currency))

  def createAccount(account: Account): Try[Unit] = DB localTx { implicit session =>
    val statement = sql"insert into Accounts values (${account.id}, ${account.balance.underlying()}, ${account.currency})"
    Try {
      statement executeUpdate () apply ()
    } map (_ => ()) rescue {
      case t: JdbcSQLException if t.getErrorCode == ErrorCode.DUPLICATE_KEY_1 => Throw(AccountAlreadyExist(account.id))
    }
  }

  def getAccount(accountID: UUID): Try[Account] = DB readOnly { implicit session =>
    val tryAccount = Try(sql"select * from Accounts where account_id = $accountID" map parseAccount single () apply () flatten)

    tryAccount map (_.toTry(AccountDoesNotExist(accountID))) flatten
  }

  def deleteAccount(accountID: UUID): Try[Unit] = DB localTx { implicit session =>
    val statement = sql"delete from Accounts where account_id = $accountID"
    Try {
      statement executeUpdate () apply ()
    } map (_ => ())
  }

  def getAccountsTransactions(accountID: UUID): Try[List[Transaction]] = DB readOnly { implicit session =>
    Try(sql"select * from Transactions where from_account = $accountID" map parseTransaction list() apply () flatten)
  }

  private def insertTransaction(transaction: Transaction)(implicit session: DBSession): Try[Unit] = {
    val statement = sql"""insert into Transactions values
      (${transaction.id}, ${transaction.fromAccount}, ${transaction.toAccount}, ${transaction.delta.amount}, ${transaction.delta.currency}, CURRENT_TIMESTAMP())"""
    Try {
      statement executeUpdate() apply()
    } map (_ => ())
  }

  private def getAccountForUpdate(accountID: UUID)(implicit session: DBSession): Try[Account] = {
    val statement = sql"select * from Accounts where account_id = $accountID for update"
    Try {
      (statement map parseAccount single () apply () flatten).toTry(AccountDoesNotExist(accountID))
    } flatten
  }

  private def checkTransactionExists(id: UUID): Try[Unit] = {
    val tryTransaction = Try(sql"select * from Transactions where transaction_id = $id" map (_ => ()) single () apply ())

    tryTransaction transform {
      case Return(Some(())) => Throw(TransactionAlreadyExist(id))
      case Return(None)     => Return(())
      case Throw(t)         => Throw(t)
    }
  }

  private def updateAccountBalance(account: Account)(implicit session: DBSession): Try[Unit] =
    Try(sql"update Accounts set balance = ${account.balance.underlying()} where account_id = ${account.id}" executeUpdate () apply ())

  def transferAmount(transaction: Transaction): Try[Unit] = DB localTx { implicit session =>
    val transactionExists = checkTransactionExists(transaction.id)
    val tryFrom           = getAccountForUpdate(transaction.fromAccount)
    val tryTo             = getAccountForUpdate(transaction.toAccount)

    (for {
      _           <- transactionExists
      fromAccount <- tryFrom
      toAccount   <- tryTo
    } yield {
      if (fromAccount.currency != transaction.delta.currency || toAccount.currency != transaction.delta.currency) {
        Throw(IncompatibleCurrency(transaction.delta.currency, fromAccount.currency, toAccount.currency))
      } else if (fromAccount.balance < transaction.delta.amount) {
        Throw(InsufficientFunds(transaction.delta, Delta(fromAccount.balance, fromAccount.currency)))
      } else {
        for {
          _ <- insertTransaction(transaction)
          _ <- updateAccountBalance(fromAccount.copy(balance = fromAccount.balance - transaction.delta.amount))
          _ <- updateAccountBalance(toAccount.copy(balance = toAccount.balance + transaction.delta.amount))
        } yield ()
      }
    }) flatten
  }

}

object Storage {
  Class.forName("org.h2.Driver")
  ConnectionPool.singleton("jdbc:h2:mem:db1", "user", "pass")

  private[storage] implicit lazy val session: DBSession = AutoSession

  sql"""
       |CREATE TABLE Accounts (
       |  account_id UUID PRIMARY KEY,
       |  balance DECIMAL(20, 4) NOT NULL,
       |  currency CHAR(3) NOT NULL
       |);
       |
       |CREATE TABLE Transactions(
       |  transaction_id UUID PRIMARY KEY,
       |  from_account UUID REFERENCES Accounts(account_id),
       |  to_account UUID REFERENCES Accounts(account_id),
       |  amount DECIMAL(20, 4) NOT NULL,
       |  currency CHAR(3) NOT NULL,
       |  created_at TIMESTAMP NOT NULL
       |);
       |
     """.stripMargin.execute.apply()

  def apply() = new Storage
}