package com.kubapiszczek.transfer.storage

import java.util.UUID

import com.kubapiszczek.transfer.model._
import com.kubapiszczek.transfer.model.errors.{AccountAlreadyExist, AccountDoesNotExist, InsufficientFunds, TransactionAlreadyExist}
import com.twitter.util.{Return, Throw, Try}
import org.scalatest.{FlatSpec, Matchers}

class StorageSpec extends FlatSpec with Matchers {

  behavior of "storage"

  val storage = Storage()

  it should "not return not existing account" in {
    val accountID = UUID.randomUUID()
    val result = storage getAccount accountID

    result shouldBe Throw(AccountDoesNotExist(accountID))
  }

  it should "insert Account" in {
    val accountID = UUID.randomUUID()
    val account = Account(accountID, BigDecimal("0.0000"), "ASD")
    val result = storage createAccount account
    val tryAccount = storage getAccount accountID

    result shouldBe Return(())
    // bigdecimal equality sucks
    tryAccount should matchPattern {
      case Return(Account(id, balance, currency)) if id == account.id && balance.compare(account.balance) == 0 && currency == account.currency =>
    }
  }

  it should "fail to insert already existsing account" in {
    val accountID = UUID.randomUUID()
    val account = Account(accountID, BigDecimal("0.0000"), "ASD")
    val result1    = storage createAccount account
    val result2    = storage createAccount account

    result1 shouldBe Return(())
    result2 shouldBe Throw(AccountAlreadyExist(accountID))
  }

  it should "transfer amount between accounts" in {
    val id1 = UUID.randomUUID()
    val id2 = UUID.randomUUID()
    val account1 = Account(id1, BigDecimal("120.0000"), "ASD")
    val account2 = Account(id2, BigDecimal("120.0000"), "ASD")

    val transactionID = UUID.randomUUID()

    val transaction = Transaction(id1, id2, transactionID, Delta(BigDecimal("50.0000"), "ASD"))

    storage createAccount account1
    storage createAccount account2

    val result = storage transferAmount transaction

    val balances: Try[(BigDecimal, BigDecimal)] = for {
      newAccount1 <- storage getAccount id1
      newAccount2 <- storage getAccount id2
    } yield {
      (newAccount1.balance, newAccount2.balance)
    }

    result shouldBe Return(())
    balances should matchPattern {
      case Return((b1: BigDecimal, b2: BigDecimal)) if b1.compare(BigDecimal("70.0000")) == 0 && b2.compare(BigDecimal("170.0000")) == 0 =>
    }
  }

  it should "fail to transfer money to not existing account" in {
    val id1 = UUID.randomUUID()
    val id2 = UUID.randomUUID()
    val account = Account(id1, BigDecimal("120.0000"), "ASD")

    val transactionID = UUID.randomUUID()

    val transaction = Transaction(id1, id2, transactionID, Delta(BigDecimal("50.0000"), "ASD"))

    storage createAccount account

    val result = storage transferAmount transaction
    val accountAfter = storage getAccount id1

    result shouldBe Throw(AccountDoesNotExist(id2))
    accountAfter should matchPattern {
      case Return(Account(id, v, _)) if id == id1 && v.compare(account.balance) == 0 =>
    }
  }

  it should "fail to transfer money from not existing account" in {
    val id1 = UUID.randomUUID()
    val id2 = UUID.randomUUID()
    val account = Account(id2, BigDecimal("120.0000"), "ASD")

    val transactionID = UUID.randomUUID()

    val transaction = Transaction(id1, id2, transactionID, Delta(BigDecimal("50.0000"), "ASD"))

    storage createAccount account

    val result = storage transferAmount transaction
    val accountAfter = storage getAccount id2

    result shouldBe Throw(AccountDoesNotExist(id1))
    accountAfter should matchPattern {
      case Return(Account(id, v, _)) if id == id2 && v.compare(account.balance) == 0 =>
    }
  }

  it should "fail to transfer amount larger than account's balance" in {
    val id1 = UUID.randomUUID()
    val id2 = UUID.randomUUID()
    val currency = "ASD"
    val amount   = BigDecimal("10.0000")
    val account1 = Account(id1, amount, currency)
    val account2 = Account(id2, BigDecimal("120.0000"), currency)

    val transactionID = UUID.randomUUID()

    val transaction = Transaction(id1, id2, transactionID, Delta(BigDecimal("50.0000"), "ASD"))

    storage createAccount account1
    storage createAccount account2

    val result = storage transferAmount transaction

    val balances: Try[(BigDecimal, BigDecimal)] = for {
      newAccount1 <- storage getAccount id1
      newAccount2 <- storage getAccount id2
    } yield {
      (newAccount1.balance, newAccount2.balance)
    }

    result shouldBe Throw(InsufficientFunds(transaction.delta, Delta(amount, currency)))
    balances should matchPattern {
      case Return((b1: BigDecimal, b2: BigDecimal)) if b1.compare(amount) == 0 && b2.compare(BigDecimal("120.0000")) == 0 =>
    }
  }

  it should "fail to create same transaction twice" in {
    val id1 = UUID.randomUUID()
    val id2 = UUID.randomUUID()
    val account1 = Account(id1, BigDecimal("120.0000"), "ASD")
    val account2 = Account(id2, BigDecimal("120.0000"), "ASD")

    val transactionID = UUID.randomUUID()

    val transaction = Transaction(id1, id2, transactionID, Delta(BigDecimal("50.0000"), "ASD"))

    storage createAccount account1
    storage createAccount account2

    val result1 = storage transferAmount transaction
    val result2 = storage transferAmount transaction

    val balances: Try[(BigDecimal, BigDecimal)] = for {
      newAccount1 <- storage getAccount id1
      newAccount2 <- storage getAccount id2
    } yield {
      (newAccount1.balance, newAccount2.balance)
    }

    result1 shouldBe Return(())
    result2 shouldBe Throw(TransactionAlreadyExist(transactionID))
    balances should matchPattern {
      case Return((b1: BigDecimal, b2: BigDecimal)) if b1.compare(BigDecimal("70.0000")) == 0 && b2.compare(BigDecimal("170.0000")) == 0 =>
    }
  }
}