package com.kubapiszczek.transfer

import com.kubapiszczek.transfer.api.RoutesImpl
import com.kubapiszczek.transfer.storage.Storage
import com.twitter.app.Flag
import com.twitter.finagle.Http
import com.twitter.server.TwitterServer
import com.twitter.util.Await

object server extends TwitterServer {
  private val storage = Storage()
  private val routesImpl = new RoutesImpl(storage)
  val apiPort = 8080

  override lazy val defaultAdminPort: Int = 8081
  val port: Flag[Int] = flag("port", apiPort, "Tcp port for HTTP endpoint of transfer service")

  def main(): Unit = {
    val server = Http.server
      .serve(s":${port()}", routesImpl.service)

    onExit {
      server close ()
    }

    Await ready adminHttpServer
  }
}
